
import {dom} from '../dom/dom';
import {ElementData} from '../dom/ElementData';
import {events} from '../events/events';
import EventManager from '../events/EventManager';

//var progress	= require('../progressbar/progressbar');

let popup =
{
	init : function(root = document)
	{
		const params				=
		{
			data_name			: 'alert',
			active_class		: 'active',
			background_id		: 'alert-background'
		};

		// if there is no alert background div on the page, add one
		if(!document.getElementById(params.background_id))
		{
			var bg		= document.createElement('div');
				bg.id	= params.background_id;

			dom.addClass(bg, params.background_id);

			document.body.appendChild(bg);
		}

		// get all of the alert items on the page
		let alerts				= root.querySelectorAll('[data-' + params.data_name + ']');

		// loop through them
		[].forEach.call(alerts, function (popup)
		{
			// default settings for the alert
			let defaults		=
			{
				// public
				"delay"			: false,
				"bg"			: false,
				"time"			: false,
				// private
				"_open"			: false,
				"_init"			: false
			};

			// the the json embedded alert data from the popup/alert
			let options			= ElementData.get(params.data_name, popup, defaults);

			// if this alert has already been initialized...
			if(options._init)
			{
				return;
			}

			// add the popup to the body to make sure there's no overlapping and
			// z-index issues
			document.body.appendChild(popup);

			var alert_interval	= null;

			/**
			 * [close description]
			 * @param  {[type]} ev [description]
			 * @return {[type]}	[description]
			 */
			var close		= function(ev)
			{
				// if an event had been passed in (assume this event was bound to a click event) cancel the event
				if(ev)
				{
					events.cancel(ev);
				}

				// the alert is no longer open
				options._open		= false;

				// remove the active class form the popup
				dom.removeClass(popup, params.active_class);

				// if there is a background displayed, hide it, and kill the noscroll
				if(options.bg)
				{
					dom.removeClass(document.body, 'noscroll');
					dom.removeClass(document.getElementById(params.background_id), params.active_class);
				}

				// if there is an overlay, kill it so it doesn't get executed
				if(alert_interval)
				{
					clearTimeout(alert_interval);
				}
			}

			/**
			 * [open description]
			 * @param  {[type]} ev [description]
			 * @return {[type]}	[description]
			 */
			var open	= function(ev = false)
			{
				// if the overlay is already open
				if(options._open)
				{
					// debugging notifications
					console.info("Alert is already open");

					// bail out as we don't want to try and re-show the alert
					return;
				}

				// make sure that the active overlay is at the bottom of the document
				// doing this will let the browser do the z-sorting for us.
				document.body.appendChild(popup);

				// create a timeout that immediately executes to allow for animatios
				// to be applied after the alert had been moved to the bottom of the body
				setTimeout(function()
				{
					// set note the state
					options._open		= true;

					// add the active class to the popup
					dom.addClass(popup, params.active_class);

					// if we shoudl show the background
					if(options.bg)
					{
						dom.addClass(document.body, 'noscroll');
						dom.addClass(document.getElementById(params.background_id), params.active_class);
					}

					// if there is a show timer...
					if(options.time)
					{
						// TODO: this properly
						/*
						var bar			= new progress(
							{
								target	: popup
							}
						);
						*/
						var start		= new Date().getTime();

						alert_interval	= setInterval(function()
						{
							var diff	= new Date().getTime() - start;
							var percent	= (diff / options.time) * 100;

							if(percent >= 100)
							{
								percent	= 100;

								close();
							}

							//bar.go(percent);

						}, 100);
					}
				}, 0);
			}

			// get all the elements in the popup that trigger the close event
			var closes			= popup.querySelectorAll('[data-close]');

			// loop through all the close triggers, and attach the close function
			[].forEach.call(closes, function (item)
			{
				events.on(item, 'click', close);
			});

			// if there is a delay on the popup, set a timer to show it
			if(options.delay !== false)
			{
				setTimeout(open, options.delay);
			}

			// update the operlay to note that it has been initialized, ideally
			// this can be avoided by calling the init method with a specific root element
			ElementData.set(params.data_name, popup, {_init: true});

			// if there is an id on the alert, add a action to allow it to be called
			if(popup.id)
			{
				EventManager.add_action(params.data_name + "#" + popup.id, open);
			}
		});
	}
}

export default popup;
